#include <stdio.h>
void change(int *value1, int *value2) {
    int tmp = &value1;
    *value1 = &value2;
    *value2 = tmp;
    printf("Value1 %d, Value2 %d\n", value1, value2);
}

int main() {
    printf("Hallo\n");
    //Compilieren mit gcc -Wall -g -o hello main.c
    // Datentypen nicht in Strings
    // Pointer sind eine Adresse
    // Call by Reference and Call by Value
    // Casten:
    // int integer b
    // float floa (float)b
    int value1 = 1;
    int value2 = 2;
    printf("Value1 %d, Value2 %d\n", value1, value2);
    change(value1, value2);
    printf("Value1 %d, Value2 %d\n", value1, value2);
    return 0;
    return 0;
}
