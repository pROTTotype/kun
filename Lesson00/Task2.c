#include <stdio.h>

// Call by Value
void swapValue(int one, int two){
    int tmp;
    tmp = one;
    one = two;
    two = tmp;
    printf("one: %d, two: %d\n", one, two);
}

// Call by Reference
void swapReference(int *one, int *two){
    int tmp;
    tmp = *one;
    *one = *two;
    *two = tmp;
}

int main() {
    // Grundlegendes Arbeiten mit Zeigern
    int *zeiger, variable;
    variable = 10;
    zeiger = &variable;
    printf("%d\n", *zeiger);


    // Methoden mit Zeigern
    int one = 1;
    int two = 2;
    printf("Swap %d with %d.\n", one , two);
    swapValue(one, two);
    printf("Swap %d with %d.\n", one , two);
    swapReference(&one, &two);
    printf("Swap %d with %d.\n", one , two);
    return 0;

}


