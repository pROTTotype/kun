/* echoserver.c */

#include <cnaiapi.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * run with ./echoserver <port>
 */

int main(int argc, char *argv[]) {

    if (argc != 2) {
        printf("Need one argument\nexit\n");
        return 1;
    }
    printf("Server up on port %s\n", argv[1]);
    int con = -1;
    printf("Waiting for connection...\n");
    short port = atoi(argv[1]);
    con = await_contact(port);

    if (con == -1) {
        printf("error\n");
        return 2;
    }
    printf("Connection established\n");


    char input[1024] = {};
    int result = 0;
    do {
        result = read(con, input, sizeof(input));
        write(con, input, sizeof(input));
    } while (result > 0);
    end_contact(con);
    printf("Server down.\n");
    return 0;
}
