/* webserver.c */
#include <cnaiapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>

int main(int argc, char *argv[]) {
    // don't delete next line
    signal(SIGCHLD, SIG_IGN);
    const char cHTTP_OK[19] = "HTTP/1.1 200 OK\r\n\r\n";
    const char cHTTP_404[26] = "HTTP/1.1 404 NOT FOUND\r\n\r\n";
    const char cIndexFile[11] = "index.html\0";
    const char cErrorFile[9] = "404.html\0";
    const char dir[8] = "htdocs/\0";
    char path[] = {"\0"};

    // check right number of arguments
    if (argc != 2) {
        printf("\tusage %s <port> \n", argv[0]);
        return 1;
    }
    short port = atoi(argv[1]);

    // waiting for connection
    int con = -1;
    int sessionID = -1;
    do {
        printf("[%d]\tWaiting for connection...\n", con);
        con = await_contact(port);
        printf("[%d] Connection created\n", con);
        if (con != -1){
            printf("[%d]\tConnection established success\n", con);
        } else {
            printf("[%d]\tConnection established with error\n", con);
            return 2;
        }
        // create sub process for request management
        sessionID = fork();
        
        switch (sessionID) {
            case -1: {
                // error
                printf("[PID:%d]\t[%d]\tError with fork\n", sessionID, con);
                return 3;
            }
            case 0: {
                // use child processes (fork || pthread || poll)
                printf("[PID:%d]\t[%d]\tChild process created\n", sessionID, con);
                int requestlength = 0;
                int packageNumber = 1;
                char input[1024] = {};
                char cache[1024] = {};
                
                while(strchr(cache, '\n') == 0 && packageNumber != 0){
                    packageNumber = read(con, cache, sizeof(cache));
                    requestlength += packageNumber;
                    strcat(input, cache);
                }
                // create end of char array
                input[requestlength] = '\0';
                printf("[PID:%d]\t[%d]\tHTTP HEADER:\n%s", sessionID, con, input);
                
                // extract request informationen
                char *reqFile = strtok(input, "/");
                reqFile = strtok(NULL, "/ ");
                
                // handle empty file name
                if (strcmp(reqFile, "HTTP") == 0){
                    strcpy(reqFile, cIndexFile);
                    // alternative: reqFile = "index.php"
                }
                strcpy(path, dir);
                strcat(path, reqFile);
                int in_fd = open(path, O_RDONLY);
                
                struct stat s;
                fstat(in_fd, &s);
                int sendfileResult = -1;
                
                if (in_fd == -1) {
                    // file not available
                    strcpy(path, dir);
                    strcat(path, cErrorFile);

                    in_fd = open(path, O_RDONLY);
                    fstat(in_fd, &s);

                    // send header + file on connection
                    write(con, cHTTP_404, 26);
                    sendfileResult = sendfile(con, in_fd, NULL, s.st_size);
                    printf("[PID:%d]\t[%d]\tResult of sending: %d\n", sessionID, con, sendfileResult);
                } else {
                    // send header + file on connection
                    write(con, cHTTP_OK, 19);
                    sendfileResult = sendfile(con, in_fd, NULL, s.st_size);
                    printf("[PID:%d]\t[%d]\tResult of sending: %d\n", sessionID, con, sendfileResult);
                }
                // close connection with child
                end_contact(con);
                return 0;
            }

            default: {
                // parent process
                // new connection was established
                printf("[PID:%d]\t[%d]\tNew Childprocess was created\n", sessionID, con);
                end_contact(con);
                break;
            }
        }
    } while (1);
}
