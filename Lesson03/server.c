#include "robot.h"

#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>

int main(int argc, char *argv[]) {
    const char cHTTP_OK[19] = "HTTP/1.1 200 OK\r\n\r\n";
    const char cDRIVE[6] = "drive?";
    const char cIMAGE[6] = "image?";
    const char cIMAGEFILE[7] = "img.bmp";
    const char cIndexFile[16] = "webcontrol.html\0";
    const char cErrorFile[9] = "404.html\0";
    const char cHTTP_404[26] = "HTTP/1.1 404 NOT FOUND\r\\r\n";
    const char dir[8] = "htdocs/\0";
    char path[] = {"\0"};

    signal(SIGPIPE, SIG_IGN);
    // check right number of arguments
    if (argc != 2) {
        printf("\tusage %s <port> \n", argv[0]);
        return 1;
    } else {
        printf("Server started success.\n");
    }
    short port = atoi(argv[1]);

    if (initializeRob() == -1) {
        printf("initializeRob(): %d\n", initializeRob());
        exit(1);
    }

    printf("id = %d\n", isRobRunning());
    int socket_, newsockfd;
    // size of adress_client of client
    socklen_t adress_len;
    // adress_client of server from client
    struct sockaddr_in serv_addr, adress_client;
    // created new socket_ (Internet IP - Protokoll Version 4 (IPv4))
    socket_ = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_ == -1) {
        printf("ERROR opening socket_");
        return 2;
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    // ip adress_client of server
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    // set port of server listening on
    serv_addr.sin_port = htons(port);

    if (bind(socket_, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) == -1) {
        printf("ERROR on binding");
        return 3;
    }
    // queue of 5 request, while serve current connection
    listen(socket_, 5);
    adress_len = sizeof(adress_client);

    while (1) {
        // accept connection
        newsockfd = accept(socket_, (struct sockaddr *) &adress_client, &adress_len);

        if (newsockfd == -1) {
            printf("ERROR on accept");
            return 4;
        }

        // buffer of request informations
        int requestlength = 0;
        int packageNumber = 1;
        char input[1024] = {};
        char cache[1024] = {};

        while (strchr(cache, '\n') == 0 && packageNumber != 0) {
            packageNumber = read(newsockfd, cache, sizeof(cache));
            requestlength += packageNumber;
            strcat(input, cache);
        }
        input[requestlength] = '\0';
        //printf("\tHTTP HEADER:\n%s", input);

        // extract request informationen
        char *request = strtok(input, "/");
        request = strtok(NULL, "/");



        // handle empty file name
        if (strcmp(request, "HTTP") == 0) {
            printf("Request is empty\n");
            strcpy(request, cIndexFile);
        }

        if (strstr(request, "webcontrol") != NULL) {
            printf("Webcontrol found\n");
            strcpy(request, cIndexFile);

            int in_fd = open(request, O_RDONLY);

            printf("Path: %s\n", path);
            struct stat s;
            fstat(in_fd, &s);
            int sendfileResult = -1;

            if (in_fd == -1) {
                // file not available
                in_fd = open(cErrorFile, O_RDONLY);
                fstat(in_fd, &s);

                // send header + file on connection
                write(newsockfd, cHTTP_404, 26);
                sendfileResult = sendfile(newsockfd, in_fd, NULL, s.st_size);
                printf("\t[%d]\tResult of sending: %d\n", newsockfd, sendfileResult);
            } else {
                // send header + file on connection
                write(newsockfd, cHTTP_OK, 19);
                sendfileResult = sendfile(newsockfd, in_fd, NULL, s.st_size);
                printf("\t[%d]\tResult of sending: %d\n", newsockfd, sendfileResult);
            }
        }

        if (strstr(request, "image") != NULL) {
            printf("Image found\n");
            strcpy(request, cIMAGEFILE);
            printf("Image BUFFER:\n%s\n", request);
            int n;
            char *buf;
            int size;
            n = getRobCamImage(&buf, &size);
            printf("%d", n);
            printf("Sending Image: %s %d\n", cIMAGEFILE, size);
            //int fd = open(cIMAGEFILE, O_RDWR);
            write(newsockfd, cHTTP_OK, 19);
            write(newsockfd, buf, size);
        }

        // searching for drive commands
        if (strstr(request, cDRIVE)) {
            printf("Drive BUFFER:\n%s\n", request);
            char *left = strstr(request, "left=") + 5;
            char *right = strstr(request, "right=") + 6;
            char left_sub[5];
            // value range: -1 to 1
            char right_sub[5];
            if (left[0] == '-') { // negative
                memcpy(left_sub, &left[0], 4);
                left_sub[4] = '\0';
            } else { // positive
                memcpy(left_sub, &left[0], 3);
                left_sub[3] = '\0';
            }
            if (right[0] == '-') { // negative
                memcpy(right_sub, &right[0], 4);
                right_sub[4] = '\0';
            } else { //positive
                memcpy(right_sub, &right[0], 3);
                right_sub[3] = '\0';
            }
            if (left[0] == '0' && left[1] != '.') {
                memcpy(left_sub, &left[0], 1);
                left_sub[1] = '\0';
            }
            if (right[0] == '0' && right[1] != '.') {
                memcpy(right_sub, &right[0], 1);
                right_sub[1] = '\0';
            }
            double finalleft = atof(left_sub);
            double finalright = atof(right_sub);
            printf("left %f\n", atof(left_sub));
            printf("right %f\n", atof(right_sub));

            // send move command to robot
            write(newsockfd, cHTTP_OK, 19);
            setRobSpeed(finalleft, finalright);
        }
        close(newsockfd);
    }
    close(socket_);

    sleep(1);    // required to perform last robot command
    shutdownRob();

    return 0;
}

