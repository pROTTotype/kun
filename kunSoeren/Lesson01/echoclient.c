/* echoclient.c */

#include <cnaiapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * run with ./echoclient <name> <port>
*/

/*
 * Theorie am 24.04.2015
 * GET/HTTP/1.0
 * -> Client an Server
 * \r\n\r\n
 *
 * -> Server an Client
 * HTTP/1.0/200 OK
 * header
 * \r\n\r\n
 * netcat
 */
int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Need two arguments\nexit\n");
        return 1;
    }
    printf("Client up\n");

    char *host = argv[1];
    short port = atoi(argv[2]);
    computer pc = cname_to_comp(host);

    printf("Try to create connection...\n");
    connection con = make_contact(pc, port);

    if (con == -1) {
        printf("error\n");
        return 1;
    }
    printf("Connection created.\n");

    while (con != -1) {
        char input[1024] = {};
        // Send request to server
        printf("Send:\n");
        read(0, input, sizeof(input));     // default io (input)
        write(con, input, sizeof(input));

        // Get answer from server
        printf("Recieved:\n");
        read(con, input, sizeof(input));
        write(1, input, sizeof(input));    // default io (output)
    }
    printf("Client down\n");
    return 0;
}
