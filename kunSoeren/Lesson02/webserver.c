/* 
webserver.c 
created by:	Sören Meißner
*/

#include <cnaiapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>

int main(int argc, char *argv[])
{

	int port = atoi(argv[1]);	// <PORT> auslesen


// Eingabe überprüfen
	if (argc != 2 || port <= 1024) 	// Eingabe falsch
	{
		printf("need <Port> (> 1024)\n");
		return 0;
	}
	else				// Eingabe korrekt
	{

	int conID = 0;
	int forkID = 0;


// endlosschleife um Server online zu halten
	do {

// auf Anfrage warten
	conID = await_contact(port);

	forkID = fork();

	if (forkID == 0)	// Kindprozess
	{	
		printf("\n\tPID Child: %d\n\n", getpid());	// PID des Kindprozesses ausgeben
		
		if(conID == -1) {	// Verbindung fehlgeschlagen
			printf("Verbindung fehlgeschlagen\n");	
			return 0;
		}
		else 			// Verbindung korrekt
		{
			printf("Verbindung erfolgreich \n\nConnection ID:  %d\n\n", conID);

// Anfrage auslesen
			int endOfRequest = 0;
			int readByte = 1;
			char buffer[1024] = "";	// Pufferspeicher für gesendete Anfrageninformationen
			char cache[1024] = "";	/* Hilfsspeicher falls Anfrageinformationen nicht mit
	 					   einmal gesendet werden können */
			while(strchr(cache, '\n') == 0 && readByte != 0)	// solange Anfrageinformationen unvollständig
			{
				readByte = read(conID, cache, sizeof(cache));
				endOfRequest += readByte;
				strcat(buffer, cache);
			}
				buffer[endOfRequest] = '\0';
				printf("Anforderungsinformationen: %s\n", buffer);
	
// angeforderte Datei herausfiltern 
			char *reqFile = strtok(buffer, "/");
			reqFile = strtok(NULL, "/ ");
			printf("eingabe: %s\n", reqFile);

			if (strcmp(reqFile, "HTTP") == 0) // keine Seite angegeben -> index öffnen
			{
				reqFile = "index.html";
			}

			char dir[] = "htdocs/";
			strcat(dir, reqFile);

// Datei zum senden vorbereiten
			int bFile = open(dir, O_RDONLY);
			
			struct stat statFile;
			fstat(bFile, &statFile);

// Datei an Client senden
			int result = 0;
			printf("\tbFile: %d\n", bFile);
			if (bFile < 0)	// Seite konnte nicht gefundenwerden
			{	
				char ErrorDir[] = "htdocs/Error404.html";
				write(conID, "HTTP/1.1 404 NOT FOUND\r\n\r\n", 26);
				int Error404File = open(ErrorDir, O_RDONLY);
			
				struct stat statFile;
				fstat(Error404File, &statFile);

				result = sendfile(conID, Error404File, NULL, statFile.st_size);	
				printf("result of sending: %d\n", result);
			}
			else 	// Seite konnte gefunden werden
			{
				write(conID, "HTTP/1.1 200 OK\r\n\r\n", 19);
				result = sendfile(conID, bFile, NULL, statFile.st_size);	
				printf("result of sending: %d\n", result);
			}

			end_contact(conID);	// Kontakt beenden
			return 1;		// Kindprozess beenden
	
		} // ende else Bedingung (Verbindung erfolgreich)
	} 
	else if (forkID > 0)	// Vaterprozess 
	{
		printf("\n\tPID Vather: %d\n\n", getpid());	// PID des Vaterprozesses ausgeben
		end_contact(conID);	// Kontaktbeenden -> Vater wartet auf neue Anfrage
	}
	else			// Error
	{
		printf("fork failed");
		return 0;
	} // ende fork-Vergleich


	} while(1);
	} // ende else Bedingung (Eingabe korrekt)

return 0;
}
