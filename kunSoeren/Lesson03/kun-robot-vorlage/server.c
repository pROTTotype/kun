#include "robot.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <signal.h>
#include <sys/sendfile.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>

#define BUF_SIZ 4096

int handle_client(const int sock)
{
// Anfrage auslesen
			sendRobHeartbeat();
			int endOfRequest = 0;
			int readByte = 1;

			char buffer[BUF_SIZ] = "";	// Pufferspeicher für gesendete Anfrageninformationen
			char cache[BUF_SIZ] = "";	/* Hilfsspeicher falls Anfrageinformationen nicht mit
				 			einmal gesendet werden können */
			char **imgData = 0;
			int *imgByte = 0;
// solange Anfrageinformationen unvollständig
			while(strchr(cache, '\n') == 0 && readByte != 0)
			{
				readByte = read(sock, cache, sizeof(cache));
				endOfRequest += readByte;
				strcat(buffer, cache);
			}
				buffer[endOfRequest] = '\0';
				printf("Anforderungsinformationen: %s\n", buffer);
	
// angeforderte Datei herausfiltern 
			char *reqFile = strtok(buffer, "/");
			reqFile = strtok(NULL, "/ ");
			printf("eingabe: %s\n", reqFile);
		// Webcontrol aufrufen
			if (strcmp(reqFile, "HTTP") == 0) // keine Seite angegeben -> index öffnen
			{
				reqFile = "webcontrol.html";
			}
		// Bild abfragen	
			if (strncmp(reqFile, "image?", 6) == 0)
			{
				FILE *fp;
				fp = fopen("img.bmp", "w");
				if (fp == -1) {
					printf("open File failed\n\n");
					return 0;
				}
				else{	
					printf("\topen File succes\n\n");

					getRobCamImage(&imgData, &imgByte);
					write(sock, imgData, imgByte);
				return 1;	
				}			
				fclose(fp);
				reqFile = "img.bmp";			
			}
		// Steuern
			if (strncmp(reqFile, "drive?", 6) == 0)
			{
				char *drivel = reqFile;
				char delimiter[] = "= &";
				char *ptr;

				ptr = strtok(drivel, delimiter);
				ptr = strtok(NULL, delimiter);
				printf("\tdrivel: %s\n", ptr);
				float speedl = atof(ptr);

				ptr = strtok(NULL, delimiter);
				ptr = strtok(NULL, delimiter);
				printf("\tdriver: %s\n", ptr);
				float speedr = atof(ptr);

				setRobSpeed(speedl, speedr);
				return 1;
			}

// Datei zum senden vorbereiten
			int bFile = open(reqFile, O_RDONLY);
			
			struct stat statFile;
			fstat(bFile, &statFile);

// Datei an Client senden
			int result = 0;
			printf("\tbFile: %d\n", bFile);

			write(sock, "HTTP/1.1 200 OK\r\n\r\n", 19);
			result = sendfile(sock, bFile, NULL, statFile.st_size);	
			printf("result of sending: %d\n", result);

			close(sock);
			return 1;
}



int main(int argc,char* argv[])
{
	signal(SIGPIPE, SIG_IGN);
	
	if(-1 == initializeRob()) {
		printf("initializeRob()\n");
		exit(1);
	}

	// TODO: code goes here ...
  	printf("id = %d\n", isRobRunning());
	int port, sock, c;
	socklen_t addr_len;
	struct sockaddr_in addr;

	port = atoi(argv[argc-1]);
	sock = socket(PF_INET, SOCK_STREAM, 0);

	printf("Port: %d\n", port);

	if(sock == -1) {
		printf("Can not create a Socket");
		exit(1);	
	}

	//socklen_t sockopt = 1;
	//setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(&sockopt));

	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(port);
	addr.sin_family = AF_INET;
	sendRobHeartbeat();
	

	// await contact
	if (bind(sock, (struct sockaddr*)&addr, sizeof(addr)) == -1)
	    {
		perror("bind() failed");
		return 2;
	    }

	if (listen(sock, 3) == -1)
	    {
		perror("listen() failed");
		return 3;
	    }

    while(1)
    {
	sendRobHeartbeat();
        addr_len = sizeof(addr);
	printf("l: %d\n", addr_len);
        c = accept(sock, (struct sockaddr*)&addr, &addr_len);
	printf("c: %d\n", c);
        if (c == -1)
        {
            perror("accept() failed");
            continue;
        }

        printf("Client from %s\n", inet_ntoa(addr.sin_addr));
        handle_client(c);

        close(c);
    }
	close(sock);

	// shutdown
	sleep(1);	// required to perform last robot command
	printf("Simulation finished\n");
	shutdownRob();

	return(0);
}

